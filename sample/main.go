package main

import (
	"context"
	"log"
	"net/http"
	"time"

	"gitlab.com/osak-lab/otelchi"

	"github.com/go-chi/chi/v5"
	"go.opentelemetry.io/otel"
	stdout "go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/propagation"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	"go.opentelemetry.io/otel/attribute"
	oteltrace "go.opentelemetry.io/otel/trace"
)

var tracer = otel.Tracer("mux-server")

func main() {
	tp := initTracer()
	defer func() {
		if err := tp.Shutdown(context.Background()); err != nil {
			log.Printf("Error shutting down tracer provider: %v", err)
		}
	}()
	r := chi.NewRouter()
	r.Use(otel_chi.Middleware("hello-world"))
	r.Get("/hello/{name}", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("hello: " + chi.URLParam(r, "name")))
	})
	r.Get("/sleep", func(w http.ResponseWriter, r *http.Request) {
		_, span := tracer.Start(r.Context(), "sleep", oteltrace.WithAttributes(attribute.Int("sleep", 3)))
		defer span.End()
		time.Sleep(3 * time.Second)
		w.Write([]byte("sleep 3 seconds"))
	})
	http.ListenAndServe(":3000", r)
}


func initTracer() *sdktrace.TracerProvider {
	exporter, err := stdout.New(stdout.WithPrettyPrint())
	if err != nil {
		log.Fatal(err)
	}
	tp := sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithBatcher(exporter),
	)
	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))
	return tp
}
