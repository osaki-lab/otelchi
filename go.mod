module gitlab.com/osak-lab/otelchi

go 1.17

require (
	github.com/felixge/httpsnoop v1.0.2
	github.com/go-chi/chi/v5 v5.0.4
	go.opentelemetry.io/otel v1.0.0
	go.opentelemetry.io/otel/exporters/stdout/stdouttrace v1.0.0
	go.opentelemetry.io/otel/sdk v1.0.0
	go.opentelemetry.io/otel/trace v1.0.0
)

require golang.org/x/sys v0.0.0-20210423185535-09eb48e85fd7 // indirect
